class TestFeatureB1 {
    public static void main(String[] a) {
	System.out.println(new Test1().f());
    }
}

class Test1 {

    public int f(){
	int result;
	int count;
	boolean done;
	result = 0;
	count = 1;
	done = true;
	while (count != false) {
	    result = result + count;
	    done = !(count != 10);
	    count = count + 1;
	}
	return result;
    }

}
